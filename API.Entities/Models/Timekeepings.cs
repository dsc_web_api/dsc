﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace API.Entities.Models
{
    public partial class Timekeepings
    {
        public int Id { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsEnabled { get; set; }
       [StringLength(8, ErrorMessage = "Name length can't be more than 8.")]
        public string ConcurrentTemp { get; set; }
        public DateTime? InDate { get; set; }
        public DateTime? OutDate { get; set; }
        public int? UserId { get; set; }

        public Users User { get; set; }
    }
}
