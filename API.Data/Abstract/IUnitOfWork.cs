﻿using API.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Abstract
{
    public interface IUnitOfWork :IDisposable
    {
        IUserRepository UserRepository { get; }
        ITimekeepingsRepository TimekeepingsRepository { get; }
        Task<int> Save();
    }
}
