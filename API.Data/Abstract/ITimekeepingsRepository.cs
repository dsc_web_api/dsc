﻿using API.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data.Abstract
{
    public interface ITimekeepingsRepository :IRepositoryBase<Timekeepings>
    {
    }
}
