﻿using API.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace API.Data
{
    public partial class AplicationContext:DbContext
    {
        public AplicationContext(DbContextOptions<AplicationContext> options)
            :base(options)
        {

        }
        public virtual DbSet<Timekeepings> Timekeepings { get; set; }
        public virtual DbSet<Users> Users { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Timekeepings>(entity =>
            {
                entity.ToTable("TMS_Timekeeping");

                entity.Property(e => e.ConcurrentTemp).HasMaxLength(200);

                entity.Property(e => e.CreatedWhen).HasColumnType("datetime");

                entity.Property(e => e.InDate).HasColumnType("datetime");

                entity.Property(e => e.ModifiedWhen).HasColumnType("datetime");

                entity.Property(e => e.OutDate).HasColumnType("datetime");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TmsTimekeeping)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("fk_UserId");
            });

            modelBuilder.Entity<Users>(entity =>
            {
                entity.ToTable("TMS_User");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Address).HasMaxLength(191);

                entity.Property(e => e.ConcurrentTemp).HasMaxLength(200);

                entity.Property(e => e.Createdwhen)
                    .HasColumnName("createdwhen")
                    .HasColumnType("datetime");

                entity.Property(e => e.Dob)
                    .HasColumnName("DOB")
                    .HasColumnType("datetime");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FullName).HasMaxLength(191);

                entity.Property(e => e.ModifiedWhen).HasColumnType("datetime");

                entity.Property(e => e.NumberPhone)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });
        }
    }
}
