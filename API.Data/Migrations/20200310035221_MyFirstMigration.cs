﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace API.Data.Migrations
{
    public partial class MyFirstMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TMS_User",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    createdwhen = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<int>(nullable: true),
                    ModifiedWhen = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<int>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    ConcurrentTemp = table.Column<string>(maxLength: 200, nullable: true),
                    Password = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    FullName = table.Column<string>(maxLength: 191, nullable: true),
                    Gender = table.Column<bool>(nullable: true),
                    DOB = table.Column<DateTime>(type: "datetime", nullable: true),
                    NumberPhone = table.Column<string>(unicode: false, maxLength: 10, nullable: true),
                    Address = table.Column<string>(maxLength: 191, nullable: true),
                    Email = table.Column<string>(unicode: false, maxLength: 50, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMS_User", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TMS_Timekeeping",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedWhen = table.Column<DateTime>(type: "datetime", nullable: true),
                    CreatedBy = table.Column<int>(nullable: true),
                    ModifiedWhen = table.Column<DateTime>(type: "datetime", nullable: true),
                    ModifiedBy = table.Column<int>(nullable: true),
                    IsEnabled = table.Column<bool>(nullable: true),
                    ConcurrentTemp = table.Column<string>(maxLength: 200, nullable: true),
                    InDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    OutDate = table.Column<DateTime>(type: "datetime", nullable: true),
                    UserId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TMS_Timekeeping", x => x.Id);
                    table.ForeignKey(
                        name: "fk_UserId",
                        column: x => x.UserId,
                        principalTable: "TMS_User",
                        principalColumn: "id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TMS_Timekeeping_UserId",
                table: "TMS_Timekeeping",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TMS_Timekeeping");

            migrationBuilder.DropTable(
                name: "TMS_User");
        }
    }
}
