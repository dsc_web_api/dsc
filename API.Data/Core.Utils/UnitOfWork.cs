﻿using API.Data.Abstract;
using API.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace API.Data.Core.Utils
{
    public class UnitOfWork : IUnitOfWork
    {
        private AplicationContext _aplicationContext;
        private UserRepository _user;
        private TimekeepingRepository _timekeeping;

        public UnitOfWork( AplicationContext aplicationContext)
        {
            _aplicationContext = aplicationContext;
        }

        public IUserRepository UserRepository
        {
            get
            {
                if (_user == null)
                {
                    _user = new UserRepository(_aplicationContext);
                }
                return _user;
            }
        }

        public ITimekeepingsRepository TimekeepingsRepository
        {
            get
            {
                if(_timekeeping == null)
                {
                    _timekeeping = new TimekeepingRepository(_aplicationContext);
                }
                return _timekeeping;
            }
        }

        public void Dispose()
        {
            if(_aplicationContext !=null)
            {
                _aplicationContext.Dispose();
            }
           
        }

        public Task<int> Save()
        {
            return _aplicationContext.SaveChangesAsync();
        }
    }
}
