﻿using API.Data.Abstract;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace API.Data.Core.Utils
{
    public class ReponsitoryBase<T> : IRepositoryBase<T> where T : class
    {
        public AplicationContext _aplicationContext { get; set; }
        public ReponsitoryBase(AplicationContext aplicationContext)
        {
            _aplicationContext = aplicationContext;
        }
        public DbSet<T> DbSet
        {
            get
            {
                return _aplicationContext.Set<T>();
            }
        }


        public void Create(T entity)
        {
            DbSet.Add(entity);
        }

        public void Delete(T entity)
        {
            DbSet.Remove(entity);
        }

        public IQueryable<T> GetAll()
        {
            return DbSet.AsQueryable();
        }

        public void Update(T entity)
        {
            //DbSet.Update(entity);
            //_aplicationContext.Entry(entity).State = EntityState.Modified;
            //DbSet.Attach(entity);
            EntityEntry dbEntityEntry = _aplicationContext.Entry<T>(entity);
            dbEntityEntry.State = EntityState.Modified;
        }

        public T GetById(int id)
        {
            return DbSet.SingleOrDefault(c => ((int)c.GetType().GetProperty("Id").GetValue(c) == id));
        }
    }
}
