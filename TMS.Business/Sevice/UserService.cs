﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using API.Data.Core.Utils;
using API.Entities.Models;
using AutoMapper;
using TMS.Business.Sevice.Abstract;
using TMS.Business.Sevice.ViewModel;

namespace TMS.Business.Sevice
{
    public class UserService : IUserService
    {
        private readonly UnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService( UnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public Users GetUsersById(int id)
        {
            return _unitOfWork.UserRepository.GetById(id);
        }

        public void Save()
        {
            _unitOfWork.Save();
        }

        public IQueryable<Users>GetUsers()
        {
            return _unitOfWork.UserRepository.GetAll();
        }

        public async Task<dynamic> CreateUsers(CreateUserViewModel model)
        {
            Users _users = _mapper.Map<Users>(model);

            _unitOfWork.UserRepository.Create(_users);
            await _unitOfWork.Save();
            return _users;
        }

        public async Task<dynamic> UpdateUsers(UpdateUserViewModel model)
        {
           // Users user = _unitOfWork.UserRepository.GetById(model.Id);
           Users user = _mapper.Map<Users>(model);
            _unitOfWork.UserRepository.Update(user);
            await _unitOfWork.Save();
            return user;
        }

        public Task<dynamic> SignIn(SignInViewModel model)
        {
            throw new NotImplementedException();
        }

        public Task<dynamic> DeleteUsers(Users model)
        {
            throw new NotImplementedException();
        }

        public void delete(Users model)
        {
            _unitOfWork.UserRepository.Delete(model);
            _unitOfWork.Save();
        }

        public Task<dynamic> ChangerPassWord(ChangePassWordViewModel model)
        {
            // Users users = GetUsersById(model.UserId);
            throw new NotImplementedException();
        }
    }
}
