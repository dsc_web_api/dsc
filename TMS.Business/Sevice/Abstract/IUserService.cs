﻿using API.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TMS.Business.Sevice.ViewModel;

namespace TMS.Business.Sevice.Abstract
{
    public interface IUserService
    {
        IQueryable<Users> GetUsers();
        Users GetUsersById(int id);
        Task<dynamic> CreateUsers(CreateUserViewModel model);
        Task<dynamic> UpdateUsers(UpdateUserViewModel model);
        Task<dynamic> DeleteUsers(Users model);
        Task<dynamic> ChangerPassWord(ChangePassWordViewModel model);
        void delete(Users model);
        Task<dynamic> SignIn(SignInViewModel model);
        void Save();
    }
}
