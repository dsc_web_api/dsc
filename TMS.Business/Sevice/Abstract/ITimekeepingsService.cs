﻿using API.Entities.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TMS.Business.ViewModel;
using TMS.Business.ViewModel.Mapping;

namespace TMS.Business.Sevice.Abstract
{
    public interface ITimekeepingsService
    {
        Task<dynamic> CreateTimeKeeping(CreateTimeKeepingViewModel model);
        Task<dynamic> UpdateTimekeepings(UpdateTimeKeepingViewModel model);
        Task delete(Timekeepings model);
        dynamic GetTimeKeepingInfo(int id);
        dynamic GetTimeKeepingList();
     
    }
}

