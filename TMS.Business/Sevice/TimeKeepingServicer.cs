﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using API.Data.Core.Utils;
using API.Entities.Models;
using AutoMapper;
using TMS.Business.Sevice.Abstract;
using TMS.Business.ViewModel;

namespace TMS.Business.Sevice
{
    public class TimeKeepingServicer : ITimekeepingsService
    {
        private readonly UnitOfWork _unitOfWork;
        private IMapper _mapper;
        public TimeKeepingServicer( UnitOfWork unitOfWork,IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async System.Threading.Tasks.Task<dynamic> CreateTimeKeeping(CreateTimeKeepingViewModel model)
        {
            Timekeepings _timekeepings = _mapper.Map<Timekeepings>(model);

            _unitOfWork.TimekeepingsRepository.Create(_timekeepings);
            await _unitOfWork.Save();
            return _timekeepings;
        }

        public void delete(Timekeepings model)
        {
            _unitOfWork.TimekeepingsRepository.Delete(model);
            _unitOfWork.Save();
        }

        public  dynamic GetTimeKeepingInfo(int id)
        {
            return _unitOfWork.TimekeepingsRepository.GetById(id);
        }

        public dynamic GetTimeKeepingList()
        {
            return _unitOfWork.TimekeepingsRepository.GetAll();
        }

        public async System.Threading.Tasks.Task<dynamic> UpdateTimekeepings(UpdateTimeKeepingViewModel model)
        {
            Timekeepings timekeepings = _mapper.Map<Timekeepings>(model);
            _unitOfWork.TimekeepingsRepository.Update(timekeepings);
            await _unitOfWork.Save();
            return timekeepings;
        }

        Task ITimekeepingsService.delete(Timekeepings model)
        {
            throw new NotImplementedException();
        }

      

        
    }
}
