﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Business.Sevice.ViewModel
{
    public class UpdateUserViewModel
    {
        public int Id { get; set; }
        public DateTime? Createdwhen { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsEnabled { get; set; }
        public string ConcurrentTemp { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public bool? Gender { get; set; }
        public DateTime? Dob { get; set; }
        public string NumberPhone { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
    }
}
