﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TMS.Business.Sevice.ViewModel
{
    public class ChangePassWordViewModel
    {
        public ChangePassWordViewModel()
        {

        }
        public int UserId { get; set; }
        public string CurrentPassWord { get; set; }
        public string NewPassWord { get; set; }
    }
}
