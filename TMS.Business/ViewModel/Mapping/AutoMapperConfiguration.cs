﻿using API.Entities.Models;
using AutoMapper;
using TMS.Business.Sevice.ViewModel;

namespace TMS.Business.ViewModel.Mapping
{
   public class AutoMapperConfiguration :Profile
    {
        public AutoMapperConfiguration()
        {
            //ViewModels to Domain
            CreateMap<Users, CreateUserViewModel>();
            CreateMap<Users, UpdateUserViewModel>();
            CreateMap<Users, SignInViewModel>();
            CreateMap<Users, ChangePassWordViewModel>();
            CreateMap<Timekeepings, CreateTimeKeepingViewModel>();
            CreateMap<Timekeepings, UpdateTimeKeepingViewModel>();


            //Domain to ViewModels
            CreateMap<CreateUserViewModel, Users>();
            CreateMap<UpdateUserViewModel, Users>();
            CreateMap<SignInViewModel, Users>();
            CreateMap<ChangePassWordViewModel, Users>();
            CreateMap<CreateTimeKeepingViewModel, Timekeepings>();
            CreateMap<UpdateTimeKeepingViewModel, Timekeepings>();
        }
       
    }
}
