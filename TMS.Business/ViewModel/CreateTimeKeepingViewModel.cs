﻿using System;
using System.Collections.Generic;
using System.Text;
using TMS.Business.Sevice.ViewModel;

namespace TMS.Business.ViewModel
{
   public class CreateTimeKeepingViewModel
    {
        
        public int Id { get; set; }
        public DateTime? CreatedWhen { get; set; }
        public int? CreatedBy { get; set; }
        public DateTime? ModifiedWhen { get; set; }
        public int? ModifiedBy { get; set; }
        public bool? IsEnabled { get; set; }
        public string ConcurrentTemp { get; set; }
        public DateTime? InDate { get; set; }
        public DateTime? OutDate { get; set; }
        public int? UserId { get; set; }
        public CreateUserViewModel User { get; set; }

    }
}
