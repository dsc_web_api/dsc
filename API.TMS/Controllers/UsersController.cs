﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Entities.Models;
using API.Data.Core.Utils;
using TMS.Business.Sevice;
using TMS.Business.Sevice.Abstract;
using TMS.Business.Sevice.ViewModel;
using Microsoft.AspNetCore.Authorization;

namespace API.TMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        public IUserService _userService;

        public UsersController(UnitOfWork unitOfWork,UserService userService)
        {
            _unitOfWork = unitOfWork;
            _userService = userService;
        }

        // GET: api/Users/AllUsers
        [HttpGet("AllUsers")]
        public IEnumerable<Users> GetAllUsers()
        {
            return _userService.GetUsers();
        }
        //[HttpPost]
        //public async Task<IActionResult> PostChangePassWord([FromBody] ChangePassWordViewModel model)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }

        //    return _userService.ChangerPassWord(model);
        //}

        //// GET: api/Users/UserById/5
        [HttpGet("UserById/{id}")]
        public async Task<IActionResult> GetUsersById([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var users = _userService.GetUsersById(id);

            if (users == null)
            {
                return NotFound();
            }

            return Ok(users);
        }

        //// PUT: api/Users/UpdateUsers/5
        [HttpPut("UpdateUsers/{id}")]
        public async Task<IActionResult> PutUpdateUsers([FromRoute] int id, [FromBody] UpdateUserViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();
            }

            //_context.Entry(users).State = EntityState.Modified;

            await _userService.UpdateUsers(model);

            //try
            //{
            //    await _unitOfWork.Save();
            //}
            //catch (DbUpdateConcurrencyException)
            //{
            //    if (!UsersExists(id))
            //    {
            //        return NotFound();
            //    }
            //    else
            //    {
            //        throw;
            //    }
            //}

            return NoContent();
        }

        //// POST: api/Users/CreateUsers
        [HttpPost("CreateUsers")]
        public async Task<IActionResult> PostCreateUsers([FromBody] CreateUserViewModel users)
        {         
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _userService.CreateUsers(users);
                //_context.Users.Add(users);
                //await _context.SaveChangesAsync();
                return CreatedAtAction("GetUsers", new { id = users.Id }, users);
            }
            catch (System.Exception)
            {

                throw;
            }
            
        }

        //// DELETE: api/Users/DeleteUsers/5
        [HttpDelete("DeleteUsers/{id}")]
        public async Task<IActionResult> DeleteRemoverUsers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var users = _userService.GetUsersById(id);
            _userService.delete(users);
            //if (users == null)
            //{
            //    return NotFound();
            //}

            //_context.Users.Remove(users);
            //await _context.SaveChangesAsync();

            return Ok(users);
        }

        //private bool UsersExists(int id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
    }
}