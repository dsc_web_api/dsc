﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using API.Data;
using API.Entities.Models;
using API.Data.Core.Utils;
using TMS.Business.Sevice;
using TMS.Business.Sevice.ViewModel;
using TMS.Business.ViewModel;

namespace API.TMS.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TimekeepingsController : ControllerBase
    {
        private readonly UnitOfWork _unitOfWork;
        private TimeKeepingServicer _timeKeepingServicer;

        public TimekeepingsController(UnitOfWork unitOfWork, TimeKeepingServicer timeKeepingServicer)
        {
            _unitOfWork = unitOfWork;
            _timeKeepingServicer = timeKeepingServicer;
        }

        // GET: api/Timekeepings
        [HttpGet]
        public IEnumerable<Timekeepings> GetTimekeepings()
        {
            return _timeKeepingServicer.GetTimeKeepingList();
        }

        // GET: api/Timekeepings/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTimekeepings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var timekeepings = _timeKeepingServicer.GetTimeKeepingInfo(id);

            if (timekeepings == null)
            {
                return NotFound();
            }

            return Ok(timekeepings);
        }

        // PUT: api/Timekeepings/5
        [HttpPut("Update/{id}")]
        public async Task<IActionResult> PutTimekeepings([FromRoute] int id, [FromBody] UpdateTimeKeepingViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != model.Id)
            {
                return BadRequest();

               
            }
            await _timeKeepingServicer.UpdateTimekeepings(model);

            return Ok();
        }

        // POST: api/Timekeepings
        [HttpPost("create")]
        public async Task<IActionResult> PostTimekeepings( [FromBody] CreateTimeKeepingViewModel timeKeepings)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                var result = _timeKeepingServicer.CreateTimeKeeping(timeKeepings);
                
                return Ok(CreatedAtAction("GetTimekeeping", new { id = timeKeepings.Id }, timeKeepings));
                //return Ok(result);
            }
            catch (System.Exception)
            {

                throw;
            }
        }

      //  DELETE: api/Timekeepings/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTimekeepings([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var timekeepings = _timeKeepingServicer.GetTimeKeepingInfo(id);
            _timeKeepingServicer.delete(timekeepings);
            return Ok(timekeepings);

        }


    } 
}