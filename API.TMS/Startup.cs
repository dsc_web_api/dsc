﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Data;
using API.Data.Abstract;
using API.Data.Core.Utils;
using API.Entities.Models;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TMS.Business.Sevice;
using TMS.Business.Sevice.Abstract;
using TMS.Business.ViewModel.Mapping;

namespace API.TMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddDbContext<AplicationContext>(options => options.
            UseSqlServer(Configuration.GetConnectionString("DefaultConnection")), ServiceLifetime.Scoped);
            services.AddTransient<IRepositoryBase<Users>, ReponsitoryBase<Users>>();
            services.AddTransient<IRepositoryBase<Timekeepings>, ReponsitoryBase<Timekeepings>>();
            services.AddTransient<UnitOfWork>();
            services.AddTransient<UserService>();
            services.AddTransient<TimeKeepingServicer>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            //var config = new AutoMapper.MapperConfiguration(cfig =>
            //{
            //    cfig.AddProfile(new AutoMapperConfiguration());
            //});
            //var mapper = config.CreateMapper();
            //services.AddSingleton(mapper);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
